

/* 1.	Write a PL/SQL block to insert the student details into the Student table until the user wishes to stop.*/
declare
id varchar2(20);
firstName varchar2(20);
lastName varchar2(20);
city varchar2(20);
state varchar2(12);
zipcode varchar2(9);
email varchar2(100);
begin
loop
        insert into student(studentid,firstname,lastname,city,state,zipcode,email)
        values (&id,&firstname,&lastname,&city,&state,&zipcode,&email);
end loop;
END;
/

/*
2.            Write a PL/SQL block to display the numbers from 1 to 50 in words.
*/
DECLARE
N NUMBER(3):=1;
BEGIN
WHILE N <=50
LOOP
N:=N+1;
DBMS_OUTPUT.PUT_LINE(TO_CHAR (TO_DATE (N, 'j'), 'jsp'));
END LOOP;
END;
/

/*
3.            Write a PL/SQL block to display the name of the employees whose salary is > 3000.
*/
DECLARE
TYPE ename_array is TABLE OF employee%ROWTYPE;
store_table ename_array:=ename_array();
BEGIN
store_table.EXTEND(10);
SELECT * BULK COLLECT INTO store_table
    FROM employee WHERE SALARY>1000;
   FOR indx IN 1 .. store_table.COUNT 
   LOOP
   DBMS_OUTPUT.PUT_LINE(store_table(indx).ename);
   END LOOP;
END;
/

/*
4.            Write a PL/SQL block to accept the Department Number from the user, check for the existence in Department table, if exist display the department details for the specified department number else display appropriate error message.
*/

DECLARE
department_record department%ROWTYPE;
department_number department.deptno%TYPE;
BEGIN
select * into department_record from department where deptno = &department_number;
DBMS_OUTPUT.PUT_LINE('deptno : '||department_record.deptno||'deptname : '||department_record.deptname||'deptlocation : '||department_record.deptlocation);
end;
/

/*
5.	Write a PL/SQL block to accept the CourseId from the user, check for the existence in Course table, if exist remove the records if the user wishes to delete else display appropriate error message.
*/
DECLARE
course_id varchar2(20);
BEGIN
delete from course where courseid = &course_id;
DBMS_OUTPUT.PUT_LINE('deleted successfully');
end;
/

/*
6.	Write a PL/SQL block to display ClassId, Grade and GradeAssigned details for the specified StudentId
*/

DECLARE
TYPE shedule_array is TABLE OF StudentSchedule%ROWTYPE;
student_shedule_table shedule_array:=shedule_array();
student_id varchar2(20);
BEGIN
student_shedule_table.EXTEND(10);
SELECT * BULK COLLECT INTO student_shedule_table
    FROM StudentSchedule WHERE studentid=&student_id;
   FOR indx IN 1 .. student_shedule_table.COUNT 
   LOOP
   DBMS_OUTPUT.PUT_LINE(student_shedule_table(indx).classid||' '||student_shedule_table(indx).grade||' '||student_shedule_table(indx).gradeassigned);
   END LOOP;
END;
/

/*
7.	Write a PL/SQL block to increase the salary of the employee by 15%, if their salary is > 15000
*/

BEGIN
update employee set salary =salary *1.15 where salary>15000
END;
/

/*
9.	Write a PL/SQL block to display the count of employees whose gender is “Male”.
*/

DECLARE
male_count number;
BEGIN
select count(*) into male_count from employee where gender='Male';
DBMS_OUTPUT.PUT_LINE(male_count);
end;
/

/* Trigger 1
1.	Write a database trigger before insert for each row on the course table not allowing transactions on Sundays and Saturdays.
*/
create or replace trigger trigger_1
                       before insert 
                          on course
                           begin

    if (to_char(sysdate,'fmday') = 'sunday') then
    raise_application_error(-20001,' Insert  Opeartion  not allow because today is sunday  ');
    else  if (to_char(sysdate,'fmday') = 'saturday') then
    raise_application_error(-20001,' Insert  Opeartion  not allow because today is saturday  ');
    
      end if;
   end if;
end;
/
/* Trigger 2
2.	Write a database trigger after update for each row giving the date and the day on which the update is performed on the class table.
*/
CREATE or REPLACE TRIGGER trigger_2 
 AFTER  
 update on class 
 FOR EACH ROW 
 BEGIN 
DBMS_OUTPUT.put_line (
TO_CHAR (SYSDATE, 
'Day, DDth Month YYYY'));
 END;
/
/* Trigger 3
3.	Write a database trigger before delete for each row not allowing deletion and giving message on the department table.
*/
CREATE or REPLACE TRIGGER trigger_3 
 BEFORE  
 delete on department 

 BEGIN 
raise_application_error(-20001,'Records can not be deleted');
 END;
 
/* Trigger 4
4.	Write a database trigger before insert/delete/update for each row not allowing any of these operations on the table student on Monday, Wednesday and Sunday.
*/
create or replace trigger trigger_4                   
             before insert or delete or update on student
           begin
         if (to_char(sysdate,'fmday') = 'sunday') then
         raise_application_error(-20001,' Insert  Opeartion  not allow because today is sunday  ');
       else  if (to_char(sysdate,'fmday') = 'saturday') then
      raise_application_error(-20001,' Insert  Opeartion  not allow because today is saturday  ');
        else  if (to_char(sysdate,'fmday') = 'monday') then
      raise_application_error(-20001,' Insert  Opeartion  not allow because today is monday  ');
      end if;
      end if;
   end if;
end;

/*Exceptions 1
1.	Write a PL/SQL block to handle the exception named “DUP_VAL_ON_INDEX by inserting a duplicate row in the course table.
*/
Declare
begin
    insert into course(courseid,deptno,title,description,additionalfees)
    values('1','22','plsql', 'database','220');
    dbms_output.put_line('Row inserted Successfully!');
exception
    when dup_val_on_index then
        raise_application_error (-20001,'DUP_VAL_ON_INDEX: '||sqlerrm ||'   Solution: change value of course_id.' );
         
         WHEN OTHERS THEN
      raise_application_error (-20002,'An error has occurred inserting in course table.');
end;
/
/*Exceptions 2
2.	Write a PL/SQL block to handle the exception named “VALUE_ERROR” by inserting the value for DeptName column of width greater than 21 into the department table.
*/
declare
begin
    insert into department(deptno,deptname,deptlocation)
    values('1','COMPUTER SCIENCE AND ENGINEERING ','Bangalore');
    dbms_output.put_line('Row inserted Successfully!');
exception
    when VALUE_ERROR THEN
           RAISE_APPLICATION_ERROR(-20001,'VALUE_ERROR: '||sqlerrm ||'  solution: make sure that width of deptname is less than 22' );
                    WHEN OTHERS THEN
      raise_application_error (-20002,'An error has occurred inserting in department table.');
end;
/

/*1.	Write a PL/SQL block to display the total salary (ie., Salary + Comm) of each employee whose comm is not null.
*/
select (salary+comm) as total_salary from employee where comm is not null
/
/*2.	Write a PL/SQL block to display the employee details, and the number of employees joined in each month.*/
SELECT TO_CHAR(HIREDATE,'MM')as Month, COUNT (*) as no_of_employees FROM EMPLOYEE 
WHERE TO_CHAR(HIREDATE,'YYYY')= TO_CHAR(SYSDATE,'YYYY') GROUP BY TO_CHAR(HIREDATE,'MM')
/
/*3.	Write a PL/SQL block to increase the additional fees by 10% and if the additional fees exceeds 100 then decrease the additional fees by 20%.*/
UPDATE  course
 SET     ADDITIONALFEES = CASE WHEN ADDITIONALFEES >100 THEN ADDITIONALFEES-(ADDITIONALFEES*20/100)
ELSE ADDITIONALFEES+(ADDITIONALFEES*10/100)
END;                       
/
                        
/*5.	Write a PL/SQL block to display the employee details whose gender is Male.*/
select * from employee where GENDER='male'
/
/*6.	Write a PL/SQL block to count the number of students available in each city.*/
SELECT city,COUNT(*) As "No of Students"
FROM student
GROUP BY city
/

/*7.	Write a PL/SQL block to display the class location details whose seating capacity is >= 200.*/
select * from classlocation where SEATINGCAPACITY > 199
/
/*8.	Write a PL/SQL block to display all the students whose grade = ‘A’.*/
 SELECT firstname,lastname,dateofbirth,city,state,grade
  FROM student s1 JOIN  STUDENTSCHEDULE s2
    ON s1.studentid = s2.STUDENTID
 WHERE grade='A'
 /
/*9.	Write a PL/SQL block to display the details of the instructor who are handling for 1st semester.*/
  SELECT firstname,lastname,telephone,fax,email,semester
  FROM instructor s1 JOIN  class s2
    ON s1.INSTRUCTORID = s2.INSTRUCTORID
 WHERE s2.semester=1
 /
/*10.	Write a PL/SQL block to display all the records available from the table named StudentSchedule.*/
Select * from studentschedule
/